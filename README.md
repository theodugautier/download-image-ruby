# Download Revolutionary Memes Ruby Class (Ruby Class To Download Image from internet)

A small ruby class to download images from a site.

## Usage

You must create an `images` folder in the directory and run the command `ruby main.rb` to download all the french 
revolutionary memes of the site https://thechomeusegoon.wordpress.com/.

## 👨‍💻 Contributing

To contribute to this repository, feel free to create a new fork of the repository and submit a pull request.
- Fork / Clone and select the main branch.
- Create a new branch in your fork.
- Make your changes.
- Commit your changes, and push them.
- Submit a Pull Request here!

## 📋 License

The extension is available as open source under the terms of the [WTFPL](http://www.wtfpl.net/).
