require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'down'

class DownloadImage
  MAX_PAGE = 12
  EXISTING_IMAGES = Dir['./images/*']
  BASE_URL = 'https://thechomeusegoon.wordpress.com'.freeze

  def call
    counter = 1

    while counter < MAX_PAGE
      analyse_url("#{BASE_URL}/page/#{counter}/")
      counter += 1
    end
  end

  private

  def analyse_url(url)
    Nokogiri::HTML(open(url)).css('.portfolio-wrapper img').each do |picture|
      file_name = file_name(picture['src'])
      next if EXISTING_IMAGES.include?("./images/#{file_name}")

      Down.download(picture['src'], destination: "./images/#{file_name}")
    end
  end

  def file_name(src)
    array_file = src.split('/')
    "#{array_file[array_file.count - 2]}-#{array_file[array_file.count - 3]}_#{array_file.last.split('?').first}"
  end
end

DownloadImage.new.call
